# Code hosting and continuous integration: some free strategies

| How to? | [**GitLab**](https://about.gitlab.com/) | **[GitHub](https://github.com/)** |
| --- | --- |--- |
| Host sources | private instance or <http://gitlab.com>, public/private projects| <http://github.com>, public projects only |
| Host large binaries | [GitLab-LFS](https://docs.gitlab.com/ee/workflow/lfs/manage_large_binaries_with_git_lfs.html) | [GitHub-LFS](https://help.github.com/articles/about-storage-and-bandwidth-usage/): < 1Go/compte |
| Collaborate | Groups, Issues, Merge request | Organizations, Issues, Pull request |
| Chat | [Mattermost](https://about.mattermost.com/) | [Gitter](https://gitter.im/) |
| Integrate	| [GitLab-CI](https://about.gitlab.com/features/gitlab-ci-cd/): gitlab-runner | [Travis-CI](https://travis-ci.org/): online, build matrix |
| | [Jenkins](https://jenkins.io/)	| [Jenkins](https://jenkins.io/) |
| Publish online | [GitLab Pages](https://docs.gitlab.com/ce/user/project/pages/index.html), hosted on your local domain | [GitHub Pages](https://pages.github.com/), hosted on <http://github.io> | 
| Use Docker | [GitLab container registry](https://about.gitlab.com/2016/05/23/gitlab-container-registry/): private | [DockerHub](https://hub.docker.com/): public
| Use an API | [REST](https://docs.gitlab.com/ce/api/) | [REST](https://developer.github.com/v3/), [GraphQL](https://developer.github.com/v4/) |

> Please contribute on <https://gitlab.math.unistra.fr/atelier-ci/synthese>